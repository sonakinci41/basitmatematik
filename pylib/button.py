import pygame

class Button():
    def __init__(self, parent, pos, scale_fac):
        super(Button,self).__init__()
        self.parent = parent
        self.pos = [int(pos[0]*scale_fac),int(pos[1]*scale_fac)]
        self.scale_fac = scale_fac
        self.pad = [0,0]
        self.text = ""
        self.text_loc = [0,0]
        self.image = None
        self.image_path = ""
        self.image_pos = "left"#right,up,down
        self.image_loc = [0,0]
        self.size = [0,0]
        self.fixed_size = None
        self.border_size = int(5*self.scale_fac)
        self.state = "free"#on,press
        self.style = {"free":(255,255,255),
                      "on":(255,0,0),
                      "press":(0,255,0),
                      "bd":(200,20,20),
                       "fg":(0,0,0)}
        self.font_info = ("freesansbold.ttf",int(40*self.scale_fac))
        self.func_name = None
        self.func_parameters = None
        self.set_font()

    def draw(self,SCREEN):
        pygame.draw.rect(SCREEN,self.style[self.state],[*self.pos,*self.size])
        pygame.draw.rect(SCREEN,self.style["bd"],[*self.pos,*self.size],self.border_size)
        if self.image != None:
            SCREEN.blit(self.image,self.image_loc)
        SCREEN.blit(self.font.render(self.text,True,self.style["fg"]),self.text_loc)

    def state_control(self,cursor_pos,is_click=False):
        if self.pos[0] <= cursor_pos[0] <= self.pos[0] + self.size[0] and\
           self.pos[1] <= cursor_pos[1] <= self.pos[1] + self.size[1]:
            if is_click:
                if self.func_name != None:
                    f = getattr(self.parent,self.func_name)
                    if self.func_parameters != None:
                        f(*self.func_parameters)
                    else:
                        f()
                self.state = "press"
            else:
                self.state = "on"
        else:
            self.state = "free"


    def set_function(self,f_name,parameters=None):
        self.func_name = f_name
        self.func_parameters = parameters

    def set_position(self,pos):
        self.pos = [int(pos[0]*self.scale_fac),int(pos[1]*self.scale_fac)]

    def get_position(self):
        return self.pos

    def set_pad(self,pad):
        self.pad = [pad[0]*self.scale_fac,pad[1]*self.scale_fac]
        self.cal_btn_size()

    def get_pad(self):
        return self.pad

    def set_text(self,text):
        self.text = text
        self.cal_btn_size()

    def get_text(self):
        return self.text

    def set_image_path(self,img_path):
        self.image_path = img_path
        self.set_image()

    def set_image(self):
        img = pygame.image.load(self.image_path)
        img_size = img.get_rect().size
        self.image = pygame.transform.scale(img,[int(img_size[0]*self.scale_fac),int(img_size[1]*self.scale_fac)])
        self.cal_btn_size()

    def get_image(self):
        return self.image

    def set_image_pos(self,img_pos):
        self.image_pos = img_pos
        self.cal_btn_size()

    def get_image_pos(self):
        return self.image_pos

    def set_fixed_size(self,f_size):
        self.fixed_size = [int(f_size[0]*self.scale_fac),int(f_size[1]*self.scale_fac)]
        self.cal_btn_size()

    def get_fixed_size(self):
        return size

    def set_border_size(self,b_size):
        self.border_size = int(b_size*self.scale_fac)

    def get_border_size(self):
        return size

    def set_style(self,style):
        self.style = style

    def get_style(self):
        return self.style

    def set_font_info(self,f_info):
        self.font_info = [f_info[0],int(f_info[1]*self.scale_fac)]

    def set_font(self):
        self.font = pygame.font.SysFont(*self.font_info)
        self.cal_btn_size()

    def cal_btn_size(self):
        """Size text_loc ve image_loc değişkenleri güncellenecek"""
        if self.text == "":
            text_size = [0,0]
        else:
            text_size = self.font.size(self.text)
        if self.image == None:
            img_size = [0,0]
        else:
            img_size = self.image.get_rect().size

        if self.fixed_size == None:
            self.size = [0,0]
            self.text_loc = [self.pos[0] + self.pad[0] + self.border_size,self.pos[1] + self.pad[1] + self.border_size]
            self.image_loc = [self.pos[0] + self.pad[0] + self.border_size,self.pos[1] + self.pad[1] + self.border_size]
            if self.image_pos == "up" or self.image_pos == "down":
                if img_size[0] > text_size[0]:
                    self.size[0] += img_size[0]
                    self.text_loc[0] += (img_size[0] - text_size[0]) / 2
                else:
                    self.size[0] += text_size[0]
                    self.image_loc[0] += (text_size[0] - img_size[0]) / 2

                self.size[1] = text_size[1] + img_size[1]
                if self.image_pos == "up":
                    self.text_loc[1] += img_size[1]
                else:
                    self.image_loc[1] += text_size[1]
            else:
                if img_size[1] > text_size[1]:
                    self.size[1] += img_size[1]
                    self.text_loc[1] += (img_size[1] - text_size[1]) / 2
                else:
                    self.size[1] += text_size[1]
                    self.image_loc[1] += (text_size[1] - img_size[1]) / 2

                self.size[0] = text_size[0] + img_size[0]
                if self.image_pos == "left":
                    self.text_loc[0] += img_size[0]
                else:
                    self.image_loc[0] += text_size[0]
            self.size[0] += (self.border_size + self.pad[0]) * 2
            self.size[1] += (self.border_size + self.pad[1]) * 2
        else:
            """Fix bir uzunluk gelirse herseyi ortalayıp göstermeye çalışacağız"""
            self.text_loc = [self.pos[0] + self.pad[0] + self.border_size,self.pos[1] + self.pad[1] + self.border_size]
            self.image_loc = [self.pos[0] + self.pad[0] + self.border_size,self.pos[1] + self.pad[1] + self.border_size]
 
            f_size_0 = self.fixed_size[0] - self.border_size - self.pad[0]
            f_size_1 = self.fixed_size[1] - self.border_size - self.pad[1]
            if self.image_pos == "up" or self.image_pos == "down":
                self.image_loc[0] += (f_size_0 - img_size[0]) / 2
                self.text_loc[0] += (f_size_0 - text_size[0]) / 2
                if self.image_pos == "up":
                    self.image_loc[1] += (f_size_1-(img_size[1]+text_size[1])) / 2
                    self.text_loc[1] += (f_size_1+text_size[1]-img_size[1]) / 2
                else:
                    self.text_loc[1] += (f_size_1-(img_size[1]+text_size[1])) / 2
                    self.image_loc[1] += (f_size_1+text_size[1]-img_size[1]) / 2
            else:
                self.image_loc[1] += (f_size_1 - img_size[1]) / 2
                self.text_loc[1] += (f_size_1 - text_size[1]) / 2
                if self.image_pos == "left":
                    self.image_loc[0] += (f_size_0-(img_size[0]+text_size[0])) / 2
                    self.text_loc[0] += (f_size_0+img_size[0]-text_size[0]) / 2
                else:
                    self.text_loc[0] += (f_size_0-(img_size[0]+text_size[0])) / 2
                    self.image_loc[0] += (f_size_0+text_size[0]-img_size[0]) / 2

            self.size = [self.fixed_size[0],self.fixed_size[1]]




            
