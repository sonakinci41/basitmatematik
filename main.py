import pygame, sys, time
from pylib import button

pygame.init()

FPS = 60
FPSCLOCK = pygame.time.Clock()
s_size = (640,480)
SCREEN = pygame.display.set_mode(s_size)

class GameLoop():
    def __init__(self):
        self.restart()

    def run(self):
        """Döngü sürekli çalışacak ve çizim update fonksiyonları buradan yönetilecek"""
        while self.game_is_run:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    sys.exit()
                elif event.type == pygame.MOUSEBUTTONDOWN:
                    for button in self.buttons:
                        button.state_control(pygame.mouse.get_pos(),True)
                elif event.type == pygame.MOUSEBUTTONUP:
                    for button in self.buttons:
                        button.state_control(pygame.mouse.get_pos())
                elif event.type == pygame.MOUSEMOTION:
                    for button in self.buttons:
                        button.state_control(pygame.mouse.get_pos())
            if self.game_is_run:
                self.update()
                self.draw()
                pygame.display.flip()
                FPSCLOCK.tick(FPS)
            else:
                break

    def update(self):
        """Çizimi güncelleme işlemlerini burada yapacağız"""
        pass

    def draw(self):
        """Çizim işlemlerini burada göndereceğiz"""
        for button in self.buttons:
            button.draw(SCREEN)

    def restart(self):
        """Fonskiyon hem yeniden başlatma işlemlerini hemde ilk atama işlemlerini yapacak"""
        self.game_is_run = False
        time.sleep(0.1)
        self.game_is_run = True
        self.buttons = []
        b_1 = button.Button(self,[50,20],1)
        b_1.set_text("Kamuran")
        b_1.set_image_path("./img/r_3.png")
        b_1.set_image_pos("right")
        b_1.set_pad([40,20])
        b_1.set_function("print_lee",["Kamuran"])
        self.buttons.append(b_1)

        b_2 = button.Button(self,[100,200],1)
        b_2.set_image_pos("right")
        b_2.set_image_path("./img/r_3.png")
        b_2.set_text("Salih")
        b_2.set_fixed_size([200,100])
        b_2.set_function("print_lee",["Salih"])
        self.buttons.append(b_2)

        b_3 = button.Button(self,[50,400],1)
        b_3.set_image_path("./img/r_3.png")
        b_3.set_image_pos("right")
        b_3.set_pad([0,0])
        b_3.set_function("print_lee",["Rocket"])
        self.buttons.append(b_3)

        self.run()

    def print_lee(self,p="EMRE"):
        print(p)




GameLoop()
